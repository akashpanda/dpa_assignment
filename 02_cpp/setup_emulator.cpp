#include<bits/stdc++.h>

void setup_emulator(char **, int );
bool if_digits_only(string);

void setup_emulator(char **argv , int argc , Register *reg , Memory *mem) {
	//cout << argc << endl;
	int initial_Address = INITIAL_STACK_ADDR;
	//int initial_Address = 240;
	//int initial_Address = 4084;
	reg->esp() = initial_Address;
	reg->ebp() = initial_Address;
	char ch1 = (argc-2);
	mem->get_value32(initial_Address+4) = ch1;
	//int32_t base_address = initial_Address-(argc-2)*4;
	int32_t base_address = (argc-2)*4;
	mem->get_value32(initial_Address+8) = base_address;
	mem->get_value32(base_address) = atoi(argv[1]);
	for(int i=3 ; i<argc ; i++) {
		int num;
		for(int j=0 ; j < 4 ; j++) {
			char ch = argv[i][j];	
			mem->get_value32(0+((i-2)*4 + j)) = ch;
		}
		mem->get_value32(base_address+((i-2)*4)) = 0+((i-2)*4);
	}
	//mem->printMem();
	//exit(1);
	// cout << (mem->get_value32(base_address)) << endl;
	// cout << (mem->get_value32(base_address+4)) << endl;
	// cout <<  (mem->get_value32(base_address+8)) << endl;
	// cout <<  (mem->get_value32(base_address+12)) << endl;
	// cout <<  (mem->get_value32(base_address+16)) << endl;
}

bool if_digits_only(string s) {
	bool has_only_digits = true;
	for (size_t n = 0; n < s.length(); n++)
	  {
	  if (!isdigit( s[ n ] ))
	    {
	    has_only_digits = false;
	    break;
	    }
	  }
	  return has_only_digits;
}
