#include<bits/stdc++.h>

using namespace std;

#include <capstone/capstone.h>

#include "constants.cpp"
#include "struct_types.cpp"
//#include "registers.cpp"
#include "memory.cpp"
#include "registers_class.cpp"
#include "instructions.cpp"
#include "setup_emulator.cpp"

#define MAX_SIZE 1024

//int counter_of_call = 0;
int counter_of_call = 0;

map<int, cs_insn> disassemble_code(char * code_buffer ,  csh handle) {
	map<int, cs_insn> mymap;
	cs_insn *insn;
	size_t count = cs_disasm(handle, (unsigned char *)code_buffer, sizeof(code_buffer) - 1, 1000, 0, &insn);
        if (count) {
                size_t j;
                for (j = 0; j < count; j++) {
			mymap[insn[j].address] = insn[j];
                }
        }	
	return mymap;
}

Register *regs = new Register();
Memory *mem = new Memory();

int main(int argc , char *argv[])
{
	csh handle;
	cs_insn *insn;
	size_t count;

	if (cs_open(CS_ARCH_X86, CS_MODE_32, &handle)) {
		printf("ERROR: Failed to initialize engine!\n");
		return -1;
	}
	cs_option(handle, CS_OPT_DETAIL , CS_OPT_ON);
	char *file_name;
	file_name = argv[1];
	ifstream infile;
	infile.open(file_name, ios::binary | ios::in);
	char code_buffer[MAX_SIZE];
	infile.read(code_buffer , MAX_SIZE);
	count = cs_disasm(handle, (unsigned char *)code_buffer, sizeof(code_buffer) - 1, 1000, 0, &insn);
	map<int , cs_insn> instructions;
        if (count) {
                size_t j;
                for (j = 0; j < count; j++) {
                        instructions[insn[j].address] = insn[j];
                        //cout << insn[j].address << " " <<  insn[j].mnemonic << " " << insn[j].op_str << endl;
                }
        }
	//map<int , cs_insn> instructions = disassemble_code(code_buffer , handle);
    cs_close(&handle);
	setup_emulator(argv , argc , regs , mem);
	//regs->printReg();
	//mem->printMem();
	regs->set_eip(atoi(argv[2]));
	while(regs->get_eip()) {
		//cout << regs->get_eip() << endl;
		execute_next_inst(instructions , regs, mem);
	}
	cout << "EAX : " << (int32_t)regs->eax() << endl;
	return (int32_t)regs->eax();
}


