typedef union value_type {
	int32_t val32;
	int16_t val16;
	int8_t val8;
}value_type;

typedef union element {
	value_type reg;
	value_type imm;
	value_type mem;
}element;

// typedef struct value_address {
// 	int32_t &add32;
// 	int16_t &add16;
// 	int8_t &add8;
// }value_address;