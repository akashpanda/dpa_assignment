#include<bits/stdc++.h>

using namespace std;

void execute_push(cs_insn, Register * , Memory *);
void execute_pop(cs_insn, Register * , Memory *);
void execute_mov(cs_insn, Register * , Memory *);
void execute_movsx(cs_insn, Register * , Memory *);
void execute_movzx(cs_insn, Register * , Memory *);
void execute_sub(cs_insn, Register * , Memory *);
void execute_add(cs_insn, Register * , Memory *);
void execute_jmp(cs_insn, Register * , Memory *);
void execute_jne(cs_insn, Register * , Memory *);
void execute_je(cs_insn, Register * , Memory *);
void execute_jle(cs_insn, Register * , Memory *);
void execute_jl(cs_insn, Register * , Memory *);
void execute_jns(cs_insn, Register * , Memory *);
void execute_and(cs_insn, Register * , Memory *);
void execute_cmp(cs_insn, Register * , Memory *);
void execute_leave(cs_insn, Register * , Memory *);
void execute_ret(cs_insn, Register * , Memory *);
void execute_shl(cs_insn, Register * , Memory *);
void execute_test(cs_insn, Register * , Memory *);
void execute_imul(cs_insn, Register * , Memory *);
void execute_call(cs_insn, Register * , Memory *);
void execute_lea(cs_insn, Register * , Memory *);
void execute_neg(cs_insn, Register * , Memory *);

value_type get_register_value(Register *, x86_reg  , int8_t );
value_type get_memory_value(x86_op_mem  , int8_t  , Memory * , Register * );
int32_t get_memory_address(x86_op_mem  , int8_t  , Memory * , Register * );
value_type figure_out_element(cs_x86_op , Register * , Memory *) ;
int32_t figure_out_address(cs_x86_op , Register * , Memory *) ; 
void figure_out_destination_and_store(cs_x86_op  , Register * , Memory * , value_type  , int8_t ); 
void store_to_register_address(Register *, x86_reg  , int8_t  , value_type ) ;
void store_to_memory_location(x86_op_mem  , int8_t  , Memory * , value_type , Register *);

void execute_next_inst(map<int , cs_insn> instructions , Register *reg , Memory *mem) {
	int32_t eip_value = reg->get_eip();
	//cout << eip_value << endl;
	cs_insn next_inst = instructions[eip_value];
	//cout << next_inst.address << endl;
	string mnemonic = next_inst.mnemonic;
	//cout << "\n" << mnemonic << " " << next_inst.op_str << endl;;
	if(mnemonic == "push") {
		execute_push(next_inst , reg , mem);
	} else if (mnemonic == "pop") {
		execute_pop(next_inst , reg , mem);
	}else if (mnemonic == "mov") {
		execute_mov(next_inst , reg , mem);
	} else if(mnemonic == "sub") {
		execute_sub(next_inst , reg , mem);
	} else if(mnemonic == "neg") {
		execute_neg(next_inst , reg , mem);
	} else if(mnemonic == "add") {
		execute_add(next_inst , reg , mem);
	} else if(mnemonic == "movzx") {
		execute_movzx(next_inst , reg , mem);
	} else if(mnemonic == "jmp") {
		execute_jmp(next_inst , reg , mem);
	} else if(mnemonic == "jne") {
		execute_jne(next_inst , reg , mem);
	} else if(mnemonic == "je") {
		execute_je(next_inst , reg , mem);
	} else if(mnemonic == "jle") {
		execute_jle(next_inst , reg , mem);
	} else if(mnemonic == "jl") {
		execute_jl(next_inst , reg , mem);
	} else if(mnemonic == "jns") {
		execute_jns(next_inst , reg , mem);
	} else if(mnemonic == "test") {
		execute_test(next_inst , reg , mem);
	} else if(mnemonic == "and") {
		execute_and(next_inst , reg , mem);
	} else if(mnemonic == "movsx") {
		execute_movsx(next_inst , reg , mem);
	} else if(mnemonic == "cmp") {
		execute_cmp(next_inst , reg , mem);
	} else if(mnemonic == "shl") {
		execute_shl(next_inst , reg , mem);
	} else if(mnemonic == "imul") {
		execute_imul(next_inst , reg , mem);
	} else if(mnemonic == "call") {
		execute_call(next_inst , reg , mem);
	} else if(mnemonic == "leave") {
		execute_leave(next_inst , reg , mem);
	} else if(mnemonic == "ret") {
		execute_ret(next_inst , reg , mem);
	} else if(mnemonic == "nop") {
		reg->set_eip(reg->get_eip()+next_inst.size);
 	} else if(mnemonic == "lea") {
		execute_lea(next_inst , reg , mem);
 	} else {
		cout << mnemonic << "--- Not found" << endl;
		exit(0);
	}
	//reg->printReg();
	//mem->printMem();
}


void execute_ret(cs_insn instruction , Register *regs , Memory *mem) {
	if(regs->call_stack.empty()) {
		regs->set_eip(0);
		return;
	} else {
		int32_t address = regs->call_stack.top();
		regs->call_stack.pop();
		int32_t temp = mem->get_value32(regs->esp());
		//cout << "Temp is : " << temp << endl;
		regs->esp() = regs->esp() + 4;
		//regs->ebp() = regs->esp();
		//regs->set_eip(address);
		//if(regs->ebp() == INITIAL_STACK_ADDR) {cout << "YOOOOO" ; exit(1);}
		if(regs->esp() == INITIAL_STACK_ADDR) {cout << "YOOOOO" ; exit(1);}
		//if(address != temp) {cout << "Not equal. Some issue see. " << endl;exit(0);}
		//regs->set_eip(temp);
		//cout << "Debug Returning to previous function call "  << address << endl;
		return;
	}
}

void execute_push(cs_insn instruction , Register *regs , Memory *mem) {
	//mem->printMem(); cout << endl<<endl;
	//cout << instruction.op_str << endl ;exit(1);
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	//int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand = x86_details.operands[0];
	value_type value = figure_out_element(operand , regs , mem);
	//cout << value.val32 << endl;
	int8_t size = operand.size;
	if(size == 4) {
		regs->esp() = regs->esp() - 4;
		//cout << (int32_t)value.val32 << endl; 
		//cout << "Location " << regs->esp() << endl;
		mem->get_value32(regs->esp()) = value.val32;
		
	} else if (size == 2) {
		regs->esp() = regs->esp() - 2;
		mem->get_value32(regs->esp()) = value.val16;
		
	} else if (size == 1) {
		regs->esp() = regs->esp() - 1;
		mem->get_value32(regs->esp()) = value.val8;
		
	}
	//mem->printMem(); exit(1);
	regs->set_eip(regs->get_eip()+inst_size);
}

void execute_pop(cs_insn instruction , Register *regs , Memory *mem) {
	//mem->printMem(); cout << endl<<endl;
	//cout << instruction.op_str << endl ;exit(1);
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	//int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand = x86_details.operands[0];
	//value_type value = figure_out_element(operand , regs , mem);
	value_type value;
	value.val32 = regs->esp();
	int8_t size = operand.size;
	
	regs->esp() = regs->esp() + 4;
	if(size == 4) {
		figure_out_destination_and_store(operand , regs, mem , value , size);	
	} else {
		cout << "pop for other sizes not implemented. ";
		exit(0) ;
	}
	//mem->printMem(); exit(1);
	regs->set_eip(regs->get_eip()+inst_size);
}

void execute_mov(cs_insn instruction , Register *regs , Memory *mem) {
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand_destination = x86_details.operands[0];
	cs_x86_op operand_source = x86_details.operands[1];
	value_type value = figure_out_element(operand_source , regs , mem);
	//cout << value.val32 << endl;
	int8_t size = operand_source.size;
	figure_out_destination_and_store(operand_destination , regs, mem , value , size);
	regs->set_eip(regs->get_eip()+inst_size);
}

void execute_movsx(cs_insn instruction , Register *regs , Memory *mem) {
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand_destination = x86_details.operands[0];
	cs_x86_op operand_source = x86_details.operands[1];
	value_type value = figure_out_element(operand_source , regs , mem);
	//cout << value.val32 << endl;
	int8_t size_source = operand_source.size;
	x86_op_type type = operand_destination.type;
	int8_t size_destination = operand_destination.size;
	if(X86_OP_IMM == type) {
		cout << "figure_out_destination_and_store imm not implemented";
		exit(1);		
	} else if(X86_OP_REG == type) {
		x86_reg reg = operand_destination.reg;
		if(size_destination == 4) {
			if(size_source == 4) {
				store_to_register_address(regs, reg , 4 , value);		
			} else if(size_source == 2) {
				int16_t val = value.val16;
				value.val32 = (int32_t)val;
				store_to_register_address(regs, reg , 4 , value);
			} else if(size_source == 1) {
				int8_t val = value.val8;
				value.val32 = (int32_t)val;
				store_to_register_address(regs, reg , 4 , value);
			}
		}
		else if(size_destination == 2) {
			if(size_source == 2) {
				store_to_register_address(regs, reg , 2 , value);		
			} else if(size_source == 1) {
				int8_t val = value.val8;
				value.val16 = (int16_t)val;
				store_to_register_address(regs, reg , 2 , value);
			}
		} else if(size_destination == 1) {
			int8_t val = value.val8;
			value.val8 = (int8_t)val;
			store_to_register_address(regs, reg , 1 , value);
		}
	} else if(X86_OP_MEM == type) {
		x86_op_mem mem_op = operand_destination.mem;
		int8_t size = operand_destination.size;
        store_to_memory_location(mem_op , size , mem , value , regs);
		//value_type = get_memory_value(mem)
	}
	//return value_addr;
	//figure_out_destination_and_store(operand_destination , regs, mem , value , size);
	regs->set_eip(regs->get_eip()+inst_size);
}

void execute_movzx(cs_insn instruction , Register *regs , Memory *mem) {
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand_destination = x86_details.operands[0];
	cs_x86_op operand_source = x86_details.operands[1];
	value_type value = figure_out_element(operand_source , regs , mem);
	//cout << value.val32 << endl;
	int8_t size_source = operand_source.size;
	x86_op_type type = operand_destination.type;
	int8_t size_destination = operand_destination.size;
	if(X86_OP_IMM == type) {
		cout << "figure_out_destination_and_store imm not implemented";
		exit(1);		
	} else if(X86_OP_REG == type) {
		x86_reg reg = operand_destination.reg;
		if(size_destination == 4) {
			if(size_source == 4) {
				store_to_register_address(regs, reg , 4 , value);		
			} else if(size_source == 2) {
				int16_t val = value.val16;
				value.val32 = (int32_t)val;
				store_to_register_address(regs, reg , 4 , value);
			} else if(size_source == 1) {
				int8_t val = value.val8;
				value.val32 = (int32_t)val;
				store_to_register_address(regs, reg , 4 , value);
			}
		}
		else if(size_destination == 2) {
			if(size_source == 2) {
				store_to_register_address(regs, reg , 2 , value);		
			} else if(size_source == 1) {
				int8_t val = value.val8;
				value.val16 = (int16_t)val;
				store_to_register_address(regs, reg , 2 , value);
			}
		} else if(size_destination == 1) {
			int8_t val = value.val8;
			value.val8 = (int8_t)val;
			store_to_register_address(regs, reg , 1 , value);
		}
	} else if(X86_OP_MEM == type) {
		x86_op_mem mem_op = operand_destination.mem;
		int8_t size = operand_destination.size;
        store_to_memory_location(mem_op , size , mem , value , regs);
		//value_type = get_memory_value(mem)
	}
	//return value_addr;
	//figure_out_destination_and_store(operand_destination , regs, mem , value , size);
	regs->set_eip(regs->get_eip()+inst_size);
}


void execute_sub(cs_insn instruction , Register *regs , Memory *mem) {
	regs->clear_flags();
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand_destination = x86_details.operands[0];
	cs_x86_op operand_source = x86_details.operands[1];
	value_type source = figure_out_element(operand_source , regs , mem);
	value_type destination = figure_out_element(operand_destination , regs , mem);
	int8_t size_source = operand_source.size;
	int8_t size_destination = operand_destination.size;
	if(size_destination == 4) {
		if(destination.val32 < source.val32) { regs->set_cf(1); } else { regs->set_cf(0); }
		destination.val32 = destination.val32 - source.val32;
		regs->set_zf(destination.val32 == 0);
		regs->set_l0(destination.val32 < 0);
		regs->set_sf(destination.val32 >> 1);
		//regs->set
	} else if (size_destination == 2) {
		if(destination.val16 < source.val16) { regs->set_cf(1); } else { regs->set_cf(0); }
		destination.val16 = destination.val16 - source.val16;
		regs->set_of(0);
		regs->set_zf(destination.val16 == 0);
		regs->set_sf(destination.val16 >> 15);
		regs->set_l0(destination.val16 < 0);
	} else if (size_destination == 1) {
		if(destination.val8 < source.val8) { regs->set_cf(1); } else { regs->set_cf(0); }
		destination.val8 = destination.val8 - source.val8;
		regs->set_of(0);
		regs->set_zf(destination.val8 == 0);
		regs->set_sf(destination.val8 >> 7);
		regs->set_l0(destination.val8 < 0);
	}
	figure_out_destination_and_store(operand_destination , regs, mem , destination , size_destination);
	regs->set_eip(regs->get_eip()+inst_size);
}


void execute_neg(cs_insn instruction , Register *regs , Memory *mem) {
	regs->clear_flags();
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand_destination = x86_details.operands[0];
	//cs_x86_op operand_source = x86_details.operands[1];
	//value_type source = figure_out_element(operand_source , regs , mem);
	value_type destination = figure_out_element(operand_destination , regs , mem);
	//int8_t size_source = operand_source.size;
	int8_t size_destination = operand_destination.size;
	if(size_destination == 4) {
		if(0 < destination.val32 ) { regs->set_cf(1); } else { regs->set_cf(0); }
		destination.val32 = 0 - destination.val32;
		regs->set_zf(destination.val32 == 0);
		regs->set_l0(destination.val32 < 0);
		regs->set_sf(destination.val32 >> 1);
		//regs->set
	} else if (size_destination == 2) {
		if(0 < destination.val16) { regs->set_cf(1); } else { regs->set_cf(0); }
		destination.val16 = 0 - destination.val16;
		regs->set_of(0);
		regs->set_zf(destination.val16 == 0);
		regs->set_sf(destination.val16 >> 15);
		regs->set_l0(destination.val16 < 0);
	} else if (size_destination == 1) {
		if(0 < destination.val8) { regs->set_cf(1); } else { regs->set_cf(0); }
		destination.val8 = 0 - destination.val8;
		regs->set_of(0);
		regs->set_zf(destination.val8 == 0);
		regs->set_sf(destination.val8 >> 7);
		regs->set_l0(destination.val8 < 0);
	}
	figure_out_destination_and_store(operand_destination , regs, mem , destination , size_destination);
	regs->set_eip(regs->get_eip()+inst_size);
}

void execute_add(cs_insn instruction , Register *regs , Memory *mem) {
	regs->clear_flags();
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand_destination = x86_details.operands[0];
	cs_x86_op operand_source = x86_details.operands[1];
	value_type source = figure_out_element(operand_source , regs , mem);
	value_type destination = figure_out_element(operand_destination , regs , mem);
	int8_t size_source = operand_source.size;
	int8_t size_destination = operand_destination.size;
	if(size_destination == 4) {
		destination.val32 = destination.val32 + source.val32;
		uint64_t val64 = destination.val32 + source.val32;
		if(destination.val32 == val64) {
			regs->set_of(0);
		} else {
			regs->set_of(1);
		}
		regs->set_zf(destination.val32 == 0);
		regs->set_sf(destination.val32>>31);
		if(destination.val32 == val64) {
			regs->set_cf(0);
		} else {
			regs->set_cf(1);
		}
	} else if (size_destination == 2) {
		destination.val16 = destination.val16 + source.val16;
		uint64_t val64 = destination.val16 + source.val16;
		if(destination.val16 == val64) {
			regs->set_of(0);
		} else {
			regs->set_of(1);
		}
		regs->set_zf(destination.val16 == 0);
		regs->set_sf(destination.val16>>15);
		if(destination.val16 == val64) {
			regs->set_cf(0);
		} else {
			regs->set_cf(1);
		}
	} else if (size_destination == 1) {
		destination.val8 = destination.val8 + source.val8;
		uint64_t val64 = destination.val8 + source.val8;
		if(destination.val8 == val64) {
			regs->set_of(0);
		} else {
			regs->set_of(1);
		}
		regs->set_zf(destination.val8 == 0);
		regs->set_sf(destination.val8>>7);
		if(destination.val8 == val64) {
			regs->set_cf(0);
		} else {
			regs->set_cf(1);
		}
	}
	figure_out_destination_and_store(operand_destination , regs, mem , destination , size_destination);
	regs->set_eip(regs->get_eip()+inst_size);
}

void execute_lea(cs_insn instruction , Register *regs , Memory *mem) {
	//cout << "executing lea" << endl;
	//cout << instruction.mnemonic << " " << instruction.op_str << endl;
	
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand_destination = x86_details.operands[0];
	cs_x86_op operand_source = x86_details.operands[1];
	int16_t source_address = figure_out_address(operand_source , regs , mem); 
	//value_type source = figure_out_element(operand_source , regs , mem);
	//value_type destination = figure_out_element(operand_destination , regs , mem);
	int8_t size_source = operand_source.size;
	int8_t size_destination = operand_destination.size;
	/*
	if(size_destination == 4) {
		destination.val32 = destination.val32 + source.val32;
		regs->set_of(0);
		regs->set_zf(destination.val32 == 0);
		regs->set_sf(destination.val32>>31);
		regs->set_cf(destination.val32 < 0);
	} else if (size_destination == 2) {
		destination.val16 = destination.val16 + source.val16;
		regs->set_of(0);
		regs->set_zf(destination.val16 == 0);
		regs->set_sf(destination.val16>>15);
		regs->set_cf(destination.val16 < 0);
	} else if (size_destination == 1) {
		destination.val8 = destination.val8 + source.val8;
		regs->set_of(0);
		regs->set_zf(destination.val8 == 0);
		regs->set_sf(destination.val8>>7);
		regs->set_cf(destination.val8 < 0);
	}
	*/
	value_type destination;
	destination.val32 = source_address;
	figure_out_destination_and_store(operand_destination , regs, mem , destination , size_destination);
	regs->set_eip(regs->get_eip()+inst_size);
}

void execute_imul(cs_insn instruction , Register *regs , Memory *mem) {
	regs->clear_flags();
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand_destination = x86_details.operands[0];
	cs_x86_op operand_source = x86_details.operands[1];
	value_type source = figure_out_element(operand_source , regs , mem);
	value_type destination = figure_out_element(operand_destination , regs , mem);
	int8_t size_source = operand_source.size;
	int8_t size_destination = operand_destination.size;
	if(size_destination == 4) {
		destination.val32 = destination.val32 * source.val32;
		regs->set_of(0);
		regs->set_zf(destination.val32 == 0);
		regs->set_sf(destination.val32>>31);
		regs->set_cf(destination.val32 < 0);
	} else if (size_destination == 2) {
		destination.val16 = destination.val16 * source.val16;
		regs->set_of(0);
		regs->set_zf(destination.val16 == 0);
		regs->set_sf(destination.val16>>15);
		regs->set_cf(destination.val16 < 0);
	} else if (size_destination == 1) {
		destination.val8 = destination.val8 * source.val8;
		regs->set_of(0);
		regs->set_zf(destination.val8 == 0);
		regs->set_sf(destination.val8>>7);
		regs->set_cf(destination.val8 < 0);
	}
	figure_out_destination_and_store(operand_destination , regs, mem , destination , size_destination);
	regs->set_eip(regs->get_eip()+inst_size);
}


void execute_and(cs_insn instruction , Register *regs , Memory *mem) {
	regs->clear_flags();
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand_destination = x86_details.operands[0];
	cs_x86_op operand_source = x86_details.operands[1];
	value_type source = figure_out_element(operand_source , regs , mem);
	value_type destination = figure_out_element(operand_destination , regs , mem);
	int8_t size_source = operand_source.size;
	int8_t size_destination = operand_destination.size;
	if(size_destination == 4) {
		destination.val32 = destination.val32 & source.val32;
		regs->set_of(0);
		regs->set_zf(destination.val32 == 0);
		regs->set_sf(destination.val32>>31);
		regs->set_cf(destination.val32 < 0);
	} else if (size_destination == 2) {
		destination.val16 = destination.val16 & source.val16;
		regs->set_of(0);
		regs->set_zf(destination.val16 == 0);
		regs->set_sf(destination.val16>>15);
		regs->set_cf(destination.val16 < 0);
	} else if (size_destination == 1) {
		destination.val8 = destination.val8 & source.val8;
		regs->set_of(0);
		regs->set_zf(destination.val8 == 0);
		regs->set_sf(destination.val8>>7);
		regs->set_cf(destination.val8 < 0);
	}
	figure_out_destination_and_store(operand_destination , regs, mem , destination , size_destination);
	regs->set_eip(regs->get_eip()+inst_size);
}

void execute_test(cs_insn instruction , Register *regs , Memory *mem) {
	regs->clear_flags();
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand_destination = x86_details.operands[0];
	cs_x86_op operand_source = x86_details.operands[1];
	value_type source = figure_out_element(operand_source , regs , mem);
	value_type destination = figure_out_element(operand_destination , regs , mem);
	int8_t size_source = operand_source.size;
	int8_t size_destination = operand_destination.size;
	if(size_destination == 4) {
		destination.val32 = destination.val32 & source.val32;
		regs->set_of(0);
		regs->set_zf(destination.val32 == 0);
		regs->set_sf(destination.val32>>31);
		regs->set_cf(0);
	} else if (size_destination == 2) {
		destination.val16 = destination.val16 & source.val16;
		regs->set_of(0);
		regs->set_zf(destination.val16 == 0);
		regs->set_sf(destination.val16>>15);
		regs->set_cf(0);
	} else if (size_destination == 1) {
		destination.val8 = destination.val8 & source.val8;
		regs->set_of(0);
		regs->set_zf(destination.val8 == 0);
		regs->set_sf(destination.val8>>7);
		regs->set_cf(0);
	}
	figure_out_destination_and_store(operand_destination , regs, mem , destination , size_destination);
	regs->set_eip(regs->get_eip()+inst_size);
}

void execute_jmp(cs_insn instruction , Register *regs , Memory *mem) {
	//regs->clear_flags();
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand = x86_details.operands[0];
	int8_t size_destination = operand.size;
	value_type source = figure_out_element(operand , regs , mem);
	if(size_destination == 4) {
		regs->set_eip(source.val32);	
	} else if (size_destination == 2) {
		regs->set_eip(source.val16);	
	} else if (size_destination == 1) {
		regs->set_eip(source.val8);	
	} else {
		cout << "execute_jmp instructions.cpp not imlmeneted yet" << endl ;exit(1);
	}
}

void execute_jne(cs_insn instruction , Register *regs , Memory *mem) {
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	bool to_jump = (regs->get_zf() == 0);
	//cout << to_jump << endl;
	if(!to_jump) {
		regs->set_eip(regs->get_eip()+inst_size);
		return;
	}
	cs_x86_op operand = x86_details.operands[0];
	int8_t size_destination = operand.size;
	value_type source = figure_out_element(operand , regs , mem);
	if(size_destination == 4) {
		regs->set_eip(source.val32);	
	} else if (size_destination == 2) {
		regs->set_eip(source.val16);	
	} else if (size_destination == 1) {
		regs->set_eip(source.val8);	
	} else {
		cout << "execute_jmp instructions.cpp not imlmeneted yet" << endl ;exit(1);
	}
}

void execute_je(cs_insn instruction , Register *regs , Memory *mem) {
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	bool to_jump = (regs->get_zf() != 0);
	if(!to_jump) {
		regs->set_eip(regs->get_eip()+inst_size);
		return;
	}
	cs_x86_op operand = x86_details.operands[0];
	int8_t size_destination = operand.size;
	value_type source = figure_out_element(operand , regs , mem);
	if(size_destination == 4) {
		regs->set_eip(source.val32);	
	} else if (size_destination == 2) {
		regs->set_eip(source.val16);	
	} else if (size_destination == 1) {
		regs->set_eip(source.val8);	
	} else {
		cout << "execute_jmp instructions.cpp not imlmeneted yet" << endl ;exit(1);
	}
}

void execute_jns(cs_insn instruction , Register *regs , Memory *mem) {
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	bool to_jump = (regs->get_sf() == 0);
	if(!to_jump) {
		regs->set_eip(regs->get_eip()+inst_size);
		return;
	}
	cs_x86_op operand = x86_details.operands[0];
	int8_t size_destination = operand.size;
	value_type source = figure_out_element(operand , regs , mem);
	if(size_destination == 4) {
		regs->set_eip(source.val32);	
	} else if (size_destination == 2) {
		regs->set_eip(source.val16);	
	} else if (size_destination == 1) {
		regs->set_eip(source.val8);	
	} else {
		cout << "execute_jmp instructions.cpp not imlmeneted yet" << endl ;exit(1);
	}
}

void execute_jle(cs_insn instruction , Register *regs , Memory *mem) {
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	bool to_jump = (regs->get_zf() == 1) || (regs->get_l0() == 1 /*!= regs->get_of()*/) ;
	//cout << "to_jump : " << to_jump << endl;
	if(!to_jump) {
		regs->set_eip(regs->get_eip()+inst_size);
		return;
	}
	cs_x86_op operand = x86_details.operands[0];
	int8_t size_destination = operand.size;
	value_type source = figure_out_element(operand , regs , mem);
	if(size_destination == 4) {
		regs->set_eip(source.val32);	
	} else if (size_destination == 2) {
		regs->set_eip(source.val16);	
	} else if (size_destination == 1) {
		regs->set_eip(source.val8);	
	} else {
		cout << "execute_jmp instructions.cpp not imlmeneted yet" << endl ;exit(1);
	}
}

void execute_jl(cs_insn instruction , Register *regs , Memory *mem) {
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	//bool to_jump = (regs->get_sf() == 1 != regs->get_of()) ;
	bool to_jump = (regs->get_l0());
	//cout << "to_jump : " << to_jump << endl;
	if(!to_jump) {
		regs->set_eip(regs->get_eip()+inst_size);
		return;
	}
	cs_x86_op operand = x86_details.operands[0];
	int8_t size_destination = operand.size;
	value_type source = figure_out_element(operand , regs , mem);
	if(size_destination == 4) {
		regs->set_eip(source.val32);	
	} else if (size_destination == 2) {
		regs->set_eip(source.val16);	
	} else if (size_destination == 1) {
		regs->set_eip(source.val8);	
	} else {
		cout << "execute_jmp instructions.cpp not imlmeneted yet" << endl ;exit(1);
	}
}

void execute_cmp(cs_insn instruction , Register *regs , Memory *mem) {
	regs->clear_flags();
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand_destination = x86_details.operands[0];
	cs_x86_op operand_source = x86_details.operands[1];
	value_type source = figure_out_element(operand_source , regs , mem);
	value_type destination = figure_out_element(operand_destination , regs , mem);
	int8_t size_source = operand_source.size;
	int8_t size_destination = operand_destination.size;
	// if(size_destination == 4) {
		// if(destination.val32 < source.val32) { regs->set_cf(1); } else { regs->set_cf(0); }
		// destination.val32 = destination.val32 - source.val32;
		// regs->set_zf(destination.val32 == 0);
		// regs->set_l0(destination.val32 < 0);
		// regs->set_sf(destination.val32 >> 1);
	// 	//regs->set
	// } else if (size_destination == 2) {
	// 	if(destination.val16 < source.val16) { regs->set_cf(1); } else { regs->set_cf(0); }
	// 	destination.val16 = destination.val16 - source.val16;
	// 	regs->set_of(0);
	// 	regs->set_zf(destination.val16 == 0);
	// 	regs->set_sf(destination.val16 >> 15);
	// 	regs->set_l0(destination.val16 < 0);
	// } else if (size_destination == 1) {
	// 	if(destination.val8 < source.val8) { regs->set_cf(1); } else { regs->set_cf(0); }
	// 	destination.val8 = destination.val8 - source.val8;
	// 	regs->set_of(0);
	// 	regs->set_zf(destination.val8 == 0);
	// 	regs->set_sf(destination.val8 >> 7);
	// 	regs->set_l0(destination.val8 < 0);
	// }
	if(size_destination == 4) {
		if(size_source == 1) {
			if(destination.val32 < source.val8) { regs->set_cf(1);	} else {regs->set_cf(0);}
			int64_t val64 = destination.val32 - source.val8;
			destination.val32 = destination.val32 - source.val8;
			if((int)val64 !=  (int)destination.val32) { regs->set_of(1);	} else {regs->set_of(0);}
		} else if(size_source == 2) {
			int64_t val64 = destination.val32 - source.val16;
			if(destination.val32 < source.val16) { regs->set_of(1);	} else {regs->set_of(0);}
			destination.val32 = destination.val32 - source.val16;
			if((int)val64 !=  (int)destination.val32) { regs->set_of(1);	} else {regs->set_of(0);}
		} else if(size_source == 4) {
			int64_t val64 = destination.val32 - source.val32;
			if(destination.val32 < source.val32) { regs->set_cf(1);	} else {regs->set_cf(0);}
			destination.val32 = destination.val32 - source.val32;
			if((int)val64 !=  (int)destination.val32) { regs->set_of(1);	} else {regs->set_of(0);}
		}
		//regs->set_of(0);
		regs->set_zf(destination.val32 == 0);
		regs->set_sf(destination.val32 >> 31);
		regs->set_l0(destination.val32 < 0);

		//regs->set_cf(destination.val32 < 0);
		//regs->set_pf(destination.val32 < 0);
	} else if (size_destination == 2) {
		if(size_source == 1) {
			if(destination.val32 < source.val32) { regs->set_cf(1);	} else {regs->set_cf(0);}
			destination.val16 = destination.val16 - source.val8;
		} else if(size_source == 2) {
			if(destination.val16 < source.val16) { regs->set_cf(1);	} else {regs->set_cf(0);}
			destination.val16 = destination.val16 - source.val16;
		} 
		//destination.val16 = destination.val16 - source.val16;
		regs->set_of(0);
		regs->set_zf(destination.val16 == 0);
		regs->set_sf(destination.val16 >> 15);
		regs->set_l0(destination.val16 < 0);
		//regs->set_cf(destination.val16 < 0);
	} else if (size_destination == 1) {
		destination.val8 = destination.val8 - source.val8;
		regs->set_of(0);
		regs->set_zf(destination.val8 == 0);
		regs->set_sf(destination.val8 >> 7 );
		regs->set_l0(destination.val8 < 0);
		//regs->set_cf(destination.val8 < 0);
		if(destination.val8 < source.val8) { regs->set_cf(1);	} else {regs->set_cf(0);}
	}

	//figure_out_destination_and_store(operand_destination , regs, mem , destination , size_destination);
	regs->set_eip(regs->get_eip()+inst_size);
}

void execute_shl(cs_insn instruction , Register *regs , Memory *mem) {
	regs->clear_flags();
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand_destination = x86_details.operands[0];
	cs_x86_op operand_source = x86_details.operands[1];
	value_type source = figure_out_element(operand_source , regs , mem);
	value_type destination = figure_out_element(operand_destination , regs , mem);
	int8_t size_source = operand_source.size;
	int8_t size_destination = operand_destination.size;
	if(size_destination == 4) {
		destination.val32 = destination.val32 << source.val32;
		regs->set_of(0);
		regs->set_zf(destination.val32 == 0);
		regs->set_sf(destination.val32 >> 31);
		regs->set_cf(destination.val32 < 0);
		//regs->set_pf(destination.val32 < 0);
	} else if (size_destination == 2) {
		destination.val16 = destination.val16 << source.val16;
		regs->set_of(0);
		regs->set_zf(destination.val16 == 0);
		regs->set_sf(destination.val16 >> 15);
		regs->set_cf(destination.val16 < 0);
	} else if (size_destination == 1) {
		destination.val8 = destination.val8 << source.val8;
		regs->set_of(0);
		regs->set_zf(destination.val8 == 0);
		regs->set_sf(destination.val8 >> 7);
		regs->set_cf(destination.val8 < 0);
	}

	figure_out_destination_and_store(operand_destination , regs, mem , destination , size_destination);
	regs->set_eip(regs->get_eip()+inst_size);
}

void execute_call(cs_insn instruction , Register *regs , Memory *mem) {
		//mem->printMem(); cout << endl<<endl;
		//cout << instruction.op_str << endl ;exit(1);
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	//int8_t op_count = (int)x86_details.op_count;
	cs_x86_op operand = x86_details.operands[0];
	regs->call_stack.push(regs->get_eip() + inst_size);
	value_type value = figure_out_element(operand , regs , mem);
	//cout <<  "Pushed to" << regs->esp() << endl;
	//mem->get_value32(regs->esp()) = regs->get_eip() + inst_size;
	//cout << "Debug : Called :"<< value.val32<< "  should return to " << regs->get_eip() + inst_size  << endl;
	regs->esp() = regs->esp() - 4;
	mem->get_value32(regs->esp()) = regs->get_eip() + inst_size;
	//cout << value.val32 <<endl;exit(1);
	regs->set_eip(value.val32);
	// int8_t size = operand.size;
	// if(size == 4) {
	// 	//cout << value.val32; exit(1);
	// 	mem->get_value32(regs->esp()) = value.val32;
	// 	regs->esp() = regs->esp() - 4;
	// } else if (size == 2) {
	// 	mem->get_value32(regs->esp()) = value.val16;
	// 	regs->esp() = regs->esp() - 2;
	// } else if (size == 1) {
	// 	mem->get_value32(regs->esp()) = value.val8;
	// 	regs->esp() = regs->esp() - 1;
	// }
	// //mem->printMem(); exit(1);
	// regs->set_eip(regs->get_eip()+inst_size);
}


void execute_leave(cs_insn instruction , Register *regs , Memory *mem) {
	//regs->clear_flags();
	int16_t inst_size = instruction.size;
	cs_detail *detail = instruction.detail;
	cs_x86 x86_details = detail->x86;
	int8_t op_count = (int)x86_details.op_count;
	regs->esp() = regs->ebp();
	regs->ebp() = mem->get_value32(regs->esp());
	regs->esp() = regs->esp() + 4;
	regs->set_eip(regs->get_eip()+inst_size);
}

void figure_out_destination_and_store(cs_x86_op operand , Register *regs , Memory *mem , value_type value , int8_t size_operand)  {
	//value_address value_addr;
	x86_op_type type = operand.type;
	int8_t size = operand.size;
	if(X86_OP_IMM == type) {
		cout << "figure_out_destination_and_store imm not implemented";
		exit(1);		
	} else if(X86_OP_REG == type) {
		x86_reg reg = operand.reg;
		store_to_register_address(regs, reg , size , value);
	} else if(X86_OP_MEM == type) {
		x86_op_mem mem_op = operand.mem;
        store_to_memory_location(mem_op , size , mem , value , regs);
		//value_type = get_memory_value(mem)
	} else {
		cout << "figure_out_destination_and_store DONT KNOW not implemented";
		exit(1);		
	}
	//return value_addr;
}

value_type figure_out_element(cs_x86_op operand , Register *regs , Memory *mem) {
	value_type value;
	x86_op_type type = operand.type;
	int8_t size = operand.size;
	if(X86_OP_IMM == type) {
		if(size == 4) {
			//cout << operand.imm << endl;
			int32_t imm = operand.imm;
			value.val32 = imm;	
		} else if(size == 2) {
			int16_t imm = (int16_t)operand.imm;
			value.val16 = imm;	
		} else if(size == 1) {
			int8_t imm = (int8_t)operand.imm;
			value.val8 = imm;	
		} else {
			cout << "Not implemented ioio" << endl;exit(1);
		}
	} else if(X86_OP_REG == type) {
		x86_reg reg = operand.reg;
		value = get_register_value(regs, reg , size);
	} else if(X86_OP_MEM == type) {
		x86_op_mem mem_op = operand.mem;
        value = get_memory_value(mem_op , size , mem , regs);
		//value_type = get_memory_value(mem)
	}
	return value;
}

int32_t figure_out_address(cs_x86_op operand , Register *regs , Memory *mem) {
	value_type value;
	x86_op_type type = operand.type;
	//cout << type << endl;
	//cout << "Type = " << type << endl;
	int8_t size = operand.size;
	if(X86_OP_IMM == type) {
		cout << "figure_out_address not implemented with immediate" << endl;
		// if(size == 4) {
		// 	cout << operand.imm << endl;
		// 	int32_t imm = operand.imm;
		// 	value.val32 = imm;	
		// } else if(size == 2) {
		// 	int16_t imm = (int16_t)operand.imm;
		// 	value.val16 = imm;	
		// } else if(size == 1) {
		// 	int8_t imm = (int8_t)operand.imm;
		// 	value.val8 = imm;	
		// } else {
		// 	cout << "Not implemented ioio" << endl;exit(1);
		// }
	} else if(X86_OP_REG == type) {
		cout << "figure_out_address not implemented with register" << endl;
		//x86_reg reg = operand.reg;
		//value = get_register_value(regs, reg , size);
	} else if(X86_OP_MEM == type) {
		x86_op_mem mem_op = operand.mem;
        int32_t address_value = get_memory_address(mem_op , size , mem , regs);
        return address_value;
		//value_type = get_memory_value(mem)
	}
}

void store_to_memory_location(x86_op_mem mem_op , int8_t size , Memory *mem , value_type value , Register * regs) {
    unsigned int base = mem_op.base;      // base register (or X86_REG_INVALID if irrelevant)
    unsigned int index = mem_op.index;     // index register (or X86_REG_INVALID if irrelevant)
    unsigned int scale = mem_op.scale;      // scale for index register
    unsigned int disp = mem_op.disp;   // displacement value
    value_type base_value;
    if(base != 0) {
    	base_value = get_register_value(regs , (x86_reg)base , 4);
    }
    else 
    	base_value.val32 = 0;
    value_type index_value;
    if(index != 0) {
 		index_value = get_register_value(regs , (x86_reg)index , 4);
    } else {
    	index_value.val32 = 0;
    }
   	unsigned int location =  base_value.val32 + index_value.val32*scale + disp;
   // cout  << location <<endl;
   // cout << "Stored to Location : " <<  location << endl;
    if(size == 4) {
    	mem->get_value32(location) = value.val32;
    } else if(size == 2) {
		mem->get_value16(location) = value.val16;
    } else if(size == 1) {
    	mem->get_value8(location) = value.val8 ;
    } else {
    	cout << "not implemented instructions.cpp" << endl ;exit(1);
    }
}

value_type get_memory_value(x86_op_mem mem_op , int8_t size , Memory *mem , Register *regs) {
	value_type value;
	value_type base_value, index_value;
    unsigned int base = mem_op.base;      // base register (or X86_REG_INVALID if irrelevant)
    if(base != 0) 
    	base_value = get_register_value(regs , (x86_reg)base , 4);
    else  {
    	base_value.val32 = 0;
    }
    unsigned int index = mem_op.index;     // index register (or X86_REG_INVALID if irrelevant)
    if(index != 0) {
    	index_value = get_register_value(regs , (x86_reg)index , 4);
    } else {
    	index_value.val32 = 0;	
    }
    int scale = mem_op.scale;      // scale for index register
    int64_t disp = mem_op.disp;   // displacement value
    unsigned int location =  base_value.val32 + index_value.val32*scale + disp;
    if(size == 4) {
    	value.val32 = mem->get_value32(location);
    } else if(size == 2) {
		value.val16 = mem->get_value16(location);
    } else if(size == 1) {
    	value.val8 = mem->get_value8(location);
    } else {
    	cout << "not implemented instructions.cpp" << endl ;exit(1);
    }
    return value;
}
int32_t get_memory_address(x86_op_mem mem_op , int8_t size , Memory *mem , Register *regs) {
	value_type value;
	value_type base_value, index_value;
    unsigned int base = mem_op.base;      // base register (or X86_REG_INVALID if irrelevant)
    if(base != 0) {
    	base_value = get_register_value(regs , (x86_reg)base , 4);
    }
    else  {
    	base_value.val32 = 0;
    }
    unsigned int index = mem_op.index;     // index register (or X86_REG_INVALID if irrelevant)
    if(index != 0) {
    	index_value = get_register_value(regs , (x86_reg)index , 4);
    } else {
    	index_value.val32 = 0;	
    }
    int scale = mem_op.scale;      // scale for index register
    int64_t disp = mem_op.disp;   // displacement value
    int32_t location =  base_value.val32 + index_value.val32*scale + disp;
    return location;
}

void store_to_register_address(Register *regs, x86_reg reg , int8_t size , value_type value) {
	//regs->printReg();
	switch(reg) {
		case X86_REG_AH:
			regs->ah() = value.val8;
			break;
		case X86_REG_AL: 
			regs->al() = value.val8;
			break;
		case X86_REG_AX:
			regs->ax() = value.val16;
			break;
		case X86_REG_EAX:
			regs->eax() = value.val32;
			break;
		case X86_REG_BH:
			regs->bh() = value.val8;
			break;
		case X86_REG_BL:
			regs->bl() = value.val8;
			break;
        case X86_REG_BX: 
        	regs->bx() = value.val16;
			break;
        case X86_REG_EBX:
        	regs->ebx() = value.val32;
			break;
        case X86_REG_CH:
			regs->ch() = value.val8;
			break;
		case X86_REG_CL:
			regs->cl() = value.val8;
			break;
        case X86_REG_CX: 
        	regs->cx() = value.val16;
			break;
        case X86_REG_ECX:
        	regs->ecx() = value.val32;
			break;
        case X86_REG_DH:
			regs->dh() = value.val8;
			break;
		case X86_REG_DL:
			regs->dl() = value.val8;
			break;
        case X86_REG_DX: 
        	regs->dx() = value.val16;
			break;
        case X86_REG_EDX:
        	regs->edx() = value.val32;
			break;
		case X86_REG_EBP:
			regs->ebp() = value.val32;
			break;
		case X86_REG_ESP:
			regs->esp() = value.val32;
			break;
		default:
			cout << "Register " << reg << " Not implemented yet" << endl;
			exit(1);
	}
}

value_type get_register_value(Register *regs, x86_reg reg , int8_t size) {
	value_type myval;

	int8_t int8t;
	int16_t int16t;
	int32_t int32t;
	//regs->printReg();
	switch(reg) {
		case X86_REG_AH:
			int8t = (int8_t)regs->ah();
			myval.val8 = int8t;
			break;
		case X86_REG_AL: 
			int8t = (int8_t)regs->al();
			myval.val8 = int8t;
			break;
		case X86_REG_AX:
			int16t = (int16_t)regs->ax();
			myval.val16 = int16t;
			break;
		case X86_REG_EAX:
			int32t = (int32_t)regs->eax();
			myval.val32 = int32t;
			break;
		case X86_REG_BH:
			int8t = (int8_t)regs->bh();
			myval.val8 = int8t;
			break;
		case X86_REG_BL:
			int8t = (int8_t)regs->bl();
			myval.val8 = int8t;
			break;
        case X86_REG_BX: 
        	int16t = (int16_t)regs->bx();
        	myval.val16 = int16t;
			break;
        case X86_REG_EBX:
        	int32t = (int32_t)regs->ebx();
        	myval.val32 = int32t;
			break;
        case X86_REG_CH:
			int8t = (int8_t)regs->ch();
			myval.val8 = int8t;
			break;
		case X86_REG_CL:
			int8t = (int8_t)regs->cl();
			myval.val8 = int8t;
			break;
        case X86_REG_CX: 
        	int16t = (int16_t)regs->cx();
        	myval.val16 = int16t;
			break;
        case X86_REG_ECX:
        	int32t = (int32_t)regs->ecx();
        	myval.val32 = int32t;
			break;
        case X86_REG_DH:
			int8t = (int8_t)regs->dh();
			myval.val8 = int8t;
			break;
		case X86_REG_DL:
			int8t = (int8_t)regs->dl();
			myval.val8 = int8t;
			break;
        case X86_REG_DX: 
        	int16t = (int16_t)regs->dx();
        	myval.val16 = int16t;
			break;
        case X86_REG_EDX:
        	int32t = (int32_t)regs->edx();
        	myval.val32 = int32t;
			break;
		case X86_REG_EBP:
			int32t = (int32_t)regs->ebp();
        	myval.val32 = int32t;
			break;
		case X86_REG_ESP:
			int32t = (int32_t)regs->esp();
        	myval.val32 = int32t;
			break;
		default:
			cout << "REgister not implemented yet" << endl; 
			exit(1);
	}
	return myval;
}
