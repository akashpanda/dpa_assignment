#include<bits/stdc++.h>
using namespace std;


class Memory{
	//map<int32_t , int8_t[4]> memory_locations;
	uint8_t memory_locations[LOCATIONS];


public:
    Memory(){
    	for(int i = 0; i <LOCATIONS ; i++) {
    		// memory_locations[i][0] = 0;
    		// memory_locations[i][1] = 0;
    		// memory_locations[i][2] = 0;
    		// memory_locations[i][3] = 0;
    		memory_locations[i] = 0;
    	}
    }
    void printMem(){
    	for(int i = 0; i <LOCATIONS ; i++) { 
    		if(i%4 == 0) {
    			cout << i << " : ";
    		}
    		cout << (int)memory_locations[i] << " " ;
    		if(i%4 == 3) {
    			cout << " -- "<< get_value32(i-3) << " --: \t";
    		}
    		if(i%16 == 15) {
    			cout << "\n";
    		}
    	} 
    }
    int32_t &get_value32(int32_t location){
    	return (int32_t &)memory_locations[location]; 
    }
    int16_t &get_value16(int32_t location){ 
    	return (int16_t &)memory_locations[location]; 
    }
    int8_t &get_value8(int32_t location){ 
    	// if(memory_locations.find(location) == memory_locations.end()) {
    	// 	int8_t temp[4] = {0,0,0,0};
    	// 	memory_locations[location] = temp;
    	// }
    	return (int8_t &)memory_locations[location]; 
    }
};



