#include<bits/stdc++.h>

class Register{
    uint8_t reg_eax[4];
    uint8_t reg_ebx[4];
    uint8_t reg_ecx[4];
    uint8_t reg_edx[4];
    uint8_t reg_ebp[4];
    uint8_t reg_esp[4];
    int32_t eip;
    
    //int32_t esp;
    
    bool of;
    bool sf;
    bool zf;
    bool af;
    bool pf;
    bool cf;

    bool l0; 
public:
	std::stack<int32_t> call_stack;

    Register(){
        reg_eax[0] = reg_eax[1] =  reg_eax[2] =  reg_eax[3] = 0;
        reg_ebx[0] = reg_ebx[1] =  reg_ebx[2] =  reg_ebx[3] = 0;
        reg_ecx[0] = reg_ecx[1] =  reg_ecx[2] =  reg_ecx[3] = 0;
        reg_edx[0] = reg_edx[1] =  reg_edx[2] =  reg_edx[3] = 0;
    }

    void clear_flags() {of = sf = zf = af = pf = cf = 0;}
    void set_of(bool value) {of = value;}
    void set_sf(bool value) {sf = value;}
    void set_zf(bool value) {zf = value;}
    void set_af(bool value) {af = value;}
    void set_pf(bool value) {pf = value;}
    void set_cf(bool value) {cf = value;}
    void set_l0(bool value) {l0 = value;}

    bool get_of() {return of;}
    bool get_sf() {return sf;}
    bool get_zf() {return zf;}
    bool get_af() {return af;}
    bool get_pf() {return pf;}
    bool get_cf() {return cf;}
    bool get_l0() {return l0;}

    int32_t &ebp(){ return (int32_t &)reg_ebp[0]; }
    int32_t &esp(){ return (int32_t &)reg_esp[0]; }

    int32_t &eax(){ return (int32_t &)reg_eax[0]; }
    int16_t &ax(){ return (int16_t &)reg_eax[0]; }
    int8_t &al(){ return (int8_t &)reg_eax[0]; }
    int8_t &ah(){ return (int8_t &)reg_eax[1]; }
    int32_t &ebx(){ return (int32_t &)reg_ebx[0]; }
    int16_t &bx(){ return (int16_t &)reg_ebx[0]; }
    int8_t &bl(){ return (int8_t &)reg_ebx[0]; }
    int8_t &bh(){ return (int8_t &)reg_ebx[1]; }
    int32_t &ecx(){ return (int32_t &)reg_ecx[0]; }
    int16_t &cx(){ return (int16_t &)reg_ecx[0]; }
    int8_t &cl(){ return (int8_t &)reg_ecx[0]; }
    int8_t &ch(){ return (int8_t &)reg_ecx[1]; }
    int32_t &edx(){ return (int32_t &)reg_edx[0]; }
    int16_t &dx(){ return (int16_t &)reg_edx[0]; }
    int8_t &dl(){ return (int8_t &)reg_edx[0]; }
    int8_t &dh(){ return (int8_t &)reg_edx[1]; }

    // void set_esp(int32_t esp_value) {
    // 	esp = esp_value;
    // }
    // int32_t get_esp() {
    // 	return esp;
    // }

    void set_eip(int value) {
		eip = value;
	}

	int32_t get_eip() {
		return eip;
	}

	void printReg() {
		cout << "eax : "; 
		for (int i= 0 ; i<4 ;i++) {
			cout << (int)reg_eax[i] << " ";
			}
        cout << eax() ;
		cout << endl;
		cout << "ebx : "; 
		for (int i= 0 ; i<4 ;i++) {
			cout << (int)reg_ebx[i] << " ";
		}
        cout << ebx() ;
		cout << endl;
		cout << "ecx : "; 
		for (int i= 0 ; i<4 ;i++) {
			cout << (int)reg_ecx[i] << " ";
		}
        cout << ecx();
		cout << endl;
		cout << "edx : "; 
		for (int i= 0 ; i<4 ;i++) {
			cout << (int)reg_edx[i] << " ";
		}
        cout << edx() ;
		cout << endl;
		cout << "esp : "; 
		for (int i= 0 ; i<4 ;i++) {
			cout << (int)reg_esp[i] << " ";
		}
        cout << esp() ;
		cout << endl;
		cout << "ebp : "; 
		for (int i= 0 ; i<4 ;i++) {
			cout << (int)reg_ebp[i] << " ";
		}
        cout << ebp() ;
		cout << endl;
		cout << "OF : " <<  get_of() << " - " ;
    	cout << "SF : " <<  get_sf() << " - " ;
    	cout << "ZF : " <<  get_zf() << " - " ;
  	  	cout << "AF : " <<  get_af() << " - " ;
    	cout << "PF : " <<  get_pf() << " - " ;
    	cout << "CF : " <<  get_cf() << " - " ;
        cout << "L0 : " <<  get_l0() << " - " ;
    	cout << endl;
	}
};



