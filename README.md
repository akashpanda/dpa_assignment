# Dynamic Program Analysis Assignments 2018

## Assignment 01
### Problem statement
Count the number of conditional and unconditional jump instructions in the given binary.

### Input Format
name of the file as argv[1]
```
executable_file.extension <input_file_name>
```

### Output Format
Only the number calculated by the program.
```
3
```
### Language Used

Python3

### Tools used

Capstone - as a disassembly framework

### Usage

```
python3 branch_instructions.py <input_file_name>
```

## References

* [Capstone python binding](https://www.capstone-engine.org/lang_python.html) - The disassembly framework used


## Contributor

* **Akash Panda** 

