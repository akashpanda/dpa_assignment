# test1.py
from capstone import *
from capstone.x86 import *
import sys

def read_file(file_name):
    f = open(file_name , mode='rb')
    return f.read()

def disassembly_code(code , starting_address):
    instructions = []
    md = Cs(CS_ARCH_X86, CS_MODE_32)
    md.detail = True
    return md.disasm(code, starting_address)

def get_branch_instruction_count(instructions):
    branch_instructions_count = 0
    for i in instructions:
       #print("%x:\t%s\t%s" %(i.address, i.mnemonic, i.op_str))
        if X86_GRP_JUMP in i.groups :
            branch_instructions_count = branch_instructions_count + 1
    return branch_instructions_count


def count_branch_instructions(file_name):
    file_contents = read_file(file_name)
    instructions = disassembly_code(file_contents , 0x1000)
    branch_instrunction_count = get_branch_instruction_count(instructions)
    return branch_instrunction_count

def main():
    if len(sys.argv) < 2 :
        print("Usage : ./branch_instructions.py <input_binary_file_name>")
        exit()

    file_name = sys.argv[1] 
    branch_instruction_count = count_branch_instructions(file_name)
    print(branch_instruction_count)

if __name__ == '__main__' :
    main()


