<?php

class StackModule
{
	const INCREMENT = 4;
	
	private static $stackElements;

	public static function top() {
		return MainMemoryModule::get(RegisterController::get('esp'));
	}

	public static function pop() {
	}

	public static function push($object) {
		
	}
}

class FloatingPointUnitStackRegister
{
	private static $top=-1;
	private static $stackElements;

	public static function push($element) {
		self::$top++;
		self::$stackElements[self::$top] = $element;
	}

	public static function top() {
		return self::$stackElements[self::$top];
	}

	public static function pop() {
		if(self::$top < 0) {
			throw new StackEmptyException();
		}
		unset(self::$stackElements[self::$top]);
		self::$top--;
	}
}
class StackModueleException extends Exception {}
class StackEmptyException extends StackModueleException {}
