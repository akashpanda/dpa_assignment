<?php

class MainMemoryController extends AbsMemoryController
{
	private static $memoryElements;
	private static $currentAllocatorMemoryLocation = 2147483648;  // 2 to power 32

	public static function store($address , $value) {
		self::$memoryElements[$address] = $value;
	}
	public static function get($address) {
		if(isset(self::$memoryElements[$address])) {
			return self::$memoryElements[$address];
		}
		self::$memoryElements[$address] = 0x0000;
		return self::$memoryElements[$address];
	}
	public static function getMemoryLocation($bytesNeeded) {
		self::$currentAllocatorMemoryLocation -= $bytesNeeded;
		return 	self::$currentAllocatorMemoryLocation;
	}
	public static function computeParityNbits($value , $noOfBits) {
		$parity = 0; 
		$n = $value;
		while ($n) 
		{ 
			$parity = !$parity; 
			$n      = $n & ($n - 1); 
		}         
		return $parity; 
	}
}

class MainMemoryControllerException extends MemoryControllerException
{}
