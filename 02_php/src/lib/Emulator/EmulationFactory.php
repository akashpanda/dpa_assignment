<?php

class EmulationFactory
{
	private static $instance;

	protected function __construct() {
	
	}
	public function getInstance() {
		if(!isset(self::$instance)) {
			self::$instance = new EmulationFactory();
		}
		return self::$instance;
	}

	public function getX86_32EmulationManager() {
		$disassembler = x86_32DisassemblerFactory::getInstance()->getDisassembler();
		$operationManager = OperationFactory::getInstance()->getOperationManager();
		return new EmulationManager($disassembler , $operationManager);
	}
}
