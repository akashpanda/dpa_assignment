<?php 

class EmulationManager
{
	private $disassembler;
	private $operationManager;

	public function __construct($disassembler , $operationManager) {
		$this->disassembler = $disassembler;
		$this->operationManager = $operationManager;
	}

	public function executeCode($code , $startAddress , $argv) {
		$instructions = $this->disassembler->disassemble($code);
		//Debug::printAllInstructions($instructions);
		$this->__setupEmulator($argv);
		// Debug::getStaticProperties('RegisterController');
		// Debug::getStaticProperties('MainMemoryController');die;
		$this->__execute($instructions , $startAddress);

	}
	
	private function __setupEmulator($argv) {
		RegisterController::store("esp" , 0xfffffff8); //Setup base pointer of stack. 
		RegisterController::store("ebp" , 0xfffffff8); //Setup base pointer of stack.
		MainMemoryController::store(RegisterController::get("esp")  , NULL , 4);
		if(count($argv) > 1) {
			$argc = count($argv) - 1;
			MainMemoryController::store(RegisterController::get("esp") + 4 , $argc , 4);
			$commandLineArguments = array();
			$i = 1;
			$location = MainMemoryController::getMemoryLocation($argc*4); //Need to store $argc elements each of 4 bytes. 
			while($argv[$i]) {
				$locationElement = MainMemoryController::getMemoryLocation(4);
				MainMemoryController::store($locationElement , $argv[$i] , 4 );
				MainMemoryController::store($location + 4*($i-1) ,  $locationElement , 4);
				//$commandLineArguments[] = $location + 4*($i-1);
				$i++;
			}
			$locationToStorePointer = MainMemoryController::getMemoryLocation(4); 
			//MainMemoryController::store($locationToStorePointer, $location);
			MainMemoryController::store(RegisterController::get("esp") + 8 , $location);
		}
	}

	private function __execute($instructions , $startAddress) {
		$nextAddress = $startAddress;
		$count = 0;
		while($nextAddress) {
			$instruction = $this->fetchNextInstruction($instructions , $nextAddress);
			print(dechex($instruction->address) . " " . $instruction->mnemonic . " " . $instruction->op_str)."\n";
			//if($count == 11) { TestVariable::$test = 1 ;}
			$nextAddress = $this->executeInstruction($instruction , $nextAddress);
			//Debug::getStaticProperties('RegisterController');Debug::getStaticProperties('MainMemoryController');
			//if($count == false) { Debug::getStaticProperties('RegisterController');Debug::getStaticProperties('MainMemoryController');die;}
			$count++;
		}
		Debug::getStaticProperties('RegisterController');Debug::getStaticProperties('MainMemoryController');die;
		echo "[eax] => " . RegisterController::get('eax');die;
		return $result; 

	}

	private function fetchNextInstruction($instructions , $address) {
		return $instructions[$address];
	}

	private function executeInstruction($instruction , $address) {
		$nextAddress =  $this->operationManager->performOperation($instruction , $address);
		if($nextAddress == -1)  {
			$offsetCount = count($instruction->bytes);
			$nextAddress = $address + $offsetCount;
		}
		return $nextAddress;
	}

}
