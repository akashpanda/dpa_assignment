<?php

class Debug
{
	public static function getStaticProperties($className) {
		$class = new ReflectionClass($className);
                $arr = $class->getStaticProperties();
                print_R($arr);
	}
	public static function printAllInstructions($instructions) {
		foreach($instructions as $instruction) {
			print(dechex($instruction->address) . " " . $instruction->mnemonic . " " . $instruction->op_str)."\n";
		}
	}
}
