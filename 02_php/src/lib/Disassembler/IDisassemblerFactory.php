<?php

interface IDisassemblerFactory 
{
	public static function getInstance();
	public function getDisassembler();
}
