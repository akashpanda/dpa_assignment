<?php

class x86_32DisassemblerFactory implements IDisassemblerFactory
{
	private static $instance;
	public static function getInstance() {
		if(self::$instance == NULL) {
			self::$instance = new x86_32DisassemblerFactory();
		}
		return self::$instance;
	}
	
	protected function __construct() {
	}

	public function getDisassembler() {
		return new x86_32Disassembler();
	}
}
