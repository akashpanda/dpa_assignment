<?php 

class Dissassembler
{
	private $mode;
	private $architecture;
	public function __construct($architecture , $mode) {
		$this->mode = $mode;
		$this->architecture = $architecture;
	}

	public function disassemble($code) {
		$handle = cs_open($this->architecture , $this->mode);
		cs_option($handle, CS_OPT_DETAIL, CS_OPT_ON);
		$insn = cs_disasm($handle, $code, 0x1000); 
		$instructions = [];
		foreach($insn as $ins) {
			$instructions[$ins->address] = $ins;
		}
		return $instructions;

	}
/*
	public function print_ins($ins)
	{
		printf("0x%x:\t%s", $ins->address, $ins->mnemonic);
		if ($ins->op_str) printf("\t\t%s", $ins->op_str);
		printf("\n");

		printf("bytes:\t%s\n", string_hex($ins->bytes));
		printf("\tsize: %s\n", count($ins->bytes));

		if (count($ins->detail->regs_read)) {
			printf("\tregisters read: %s\n", implode(" ", $ins->detail->regs_read));
		}
		if (count($ins->detail->regs_write)) {
			printf("\tregisters modified: %s\n", implode(" ", $ins->detail->regs_write));
		}
		if (count($ins->detail->groups)) {
			printf("\tinstructions groups: %s\n",
				implode(" ", $ins->detail->groups));
		}
	}
*/
}
