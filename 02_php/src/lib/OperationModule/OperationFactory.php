<?php

class OperationFactory
{
	private static $instance;
	public function getInstance() {
		if(!isset(self::$instance)) {
			self::$instance = new OperationFactory();
		}
		return self::$instance;
	}

	protected function __construct() {
	}

	public function getOperationManager() {
		$arrOperations = $this->getOperations();
		return new OperationManager($arrOperations);
	}
	private function getOperations() {
		$operations = array();
		$operations['lea'] = new LeaOperation();
		$operations['and'] = new AndOperation();
		$operations['push'] = new PushOperation();
		$operations['mov'] = new MovOperation();
		$operations['sub'] = new SubOperation();
		$operations['add'] = new AddOperation();
		$operations['jmp'] = new JmpOperation();
		$operations['jne'] = new JneOperation();
		$operations['shl'] = new ShlOperation();
		$operations['fild'] = new FildOperation();
		$operations['fstp'] = new FstpOperation();
		$operations['cmp'] = new CmpOperation();
		$operations['je'] = new JeOperation();
		$operations['jle'] = new JleOperation();
		$operations['leave'] = new LeaveOperation();
		$operations['ret'] = new RetOperation();
		$operations['imul'] = new ImulOperation();
		return $operations;
	}
}
