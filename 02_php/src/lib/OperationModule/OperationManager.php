<?php

class OperationManager
{
	private $arrOperations; 

	public function __construct($arrOperations) {
		$this->arrOperations = $arrOperations;
	}

	public function performOperation($instruction , $currentAddress) {
		$mnemonic = $instruction->mnemonic;
		$returnValue = "";
		switch($mnemonic) {
			case "movzx":
			case "movsx":
				$returnValue = $this->arrOperations['mov']->perform($instruction);
				break;
			case "test":
				$returnValue = $this->arrOperations['and']->perform($instruction);
				break;
			default:
				$returnValue = $this->arrOperations[$mnemonic]->perform($instruction);
				break;
			// case "lea":
			// 	$returnValue = $this->execute_lea($instruction);
			// 	break;
			// case "and":
			// 	$returnValue = $this->execute_and($instruction);
			// 	break;
			// case "push":
			// 	$returnValue = $this->execute_push($instruction);
			// 	break;
			// case "mov":
			// 	$returnValue = $this->execute_mov($instruction);
			// 	break;
			// case "sub":
			// 	$returnValue = $this->execute_sub($instruction);
			// 	break;
			// case "add":
			// 	$returnValue = $this->execute_add($instruction);
			// 	break;
		}
		$this->__executePostOperation($instruction);
		return $returnValue;
	}

	// private function execute_lea($instruction) {
	// 	return $this->arrOperations['lea']->perform($instruction);
	// }

	// private function execute_and($instruction) {
	// 	return $this->arrOperations['and']->perform($instruction);
	// }
	// private function execute_push($instruction) {
	// 	return $this->arrOperations['push']->perform($instruction);
	// }
	// private function execute_mov($instruction) {
	// 	return $this->arrOperations['mov']->perform($instruction);
	// }
	// private function execute_sub($instruction) {
	// 	return $this->arrOperations['sub']->perform($instruction);	
	// }
	private function __executePostOperation($instruction) {
		
	}
}
