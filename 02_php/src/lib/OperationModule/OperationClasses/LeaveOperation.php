<?php

class LeaveOperation extends BaseOperation implements IOperation
{
	public function perform($instruction) {
		RegisterController::store("esp" , RegisterController::get("ebp"));
		RegisterController::store("ebp" ,  MainMemoryController::get(RegisterController::get("esp")));
		RegisterController::store("esp" , RegisterController::get("esp")-4);
		return -1;
	}
}
