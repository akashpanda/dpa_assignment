<?php

class PushOperation extends BaseOperation implements IOperation
{
	public function perform($instruction) {
		RegisterController::store('eflags' , 0);
		$operands = $instruction->detail->x86->operands;
		$source = $operands[0];
		$element = $this->figureOutElement($source);
		MainMemoryController::store(RegisterController::get('esp') , $element);
		RegisterController::store('esp' , RegisterController::get('esp')-4);
		// Debug::getStaticProperties('RegisterController');
		// Debug::getStaticProperties('MainMemoryController');die;
		return -1;
	}
}
