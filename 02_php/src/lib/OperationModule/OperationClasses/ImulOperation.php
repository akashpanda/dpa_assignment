<?php
class ImulOperation extends BaseOperation implements IOperation
{
        public function perform($instruction) {
                RegisterController::resetFlags();
                $operands = $instruction->detail->x86->operands;
               	switch (count($operands)) {
               		case 1:
               			$source = $operands[0];
               			$sourceValue = $this->figureOutElement($source);

               			$destinationCurrentValue = $this->figureOutElement($destination);
               			$temporary = $destinationCurrentValue * $sourceValue;
               			$storeResponse = $this->storeToDestination($destination , $temporary);
               			$this->setFlags($destination , $instruction , $storeResponse);
               			break;
               		case 2:
               			$source = $operands[1];
               			switch ($source->size) {
               				case 1:
               					$sourceValue = $this->figureOutElement($source);
               					$alValue = RegisterController::get('al') * $sourceValue;
               					$storeResponse = RegisterController::store('ax' , $alValue);
               					if(RegisterController::get('al') != RegisterController::get('ax')) {
               						$storeResponse['carryFlag'] = 1;
               						$storeResponse['overflow'] = 1;
               					}
               					$this->setFlags('al' , $instruction , $storeResponse , 'reg');
               					break;
               				case 2:
               					$sourceValue = $this->figureOutElement($source);
               					$alValue = RegisterController::get('ax') * $sourceValue;
               					$storeResponse = RegisterController::store('ax' , $alValue);
               					$this->setFlags('ax' , $instruction , $storeResponse , 'reg');
               					break;
               				case 4:
               					$sourceValue = $this->figureOutElement($source);
               					$alValue = RegisterController::get('eax') * $sourceValue;
               					$storeResponse = RegisterController::store('eax' , $alValue);
               					$this->setFlags('eax' , $instruction , $storeResponse , 'reg');
               					break;
               				default:
               					# code...
               					break;
               			}
               			break;
               		case 3:
               			$source1 = $operands[1];
               			$souece2 =  $operands[2];
               			$destination = $operands[0];
               			$source1Value = $this->figureOutElement($source1);
						$source2Value = $this->figureOutElement($source2);
               			$temporary = $source1Value * $source2Value;
               			$storeResponse = $this->storeToDestination($destination , $temporary);
               			$this->setFlags($destination , $instruction , $storeResponse);
               			break;
               		default:
               			# code...
               			break;
               	}
                return -1;
        }
}
