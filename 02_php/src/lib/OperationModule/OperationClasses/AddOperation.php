<?php

class AddOperation extends BaseOperation implements IOperation
{
	public function perform($instruction) {
		RegisterController::resetFlags();
		$operands = $instruction->detail->x86->operands;
		$destination = $operands[0];
		$source = $operands[1];
		$element = $this->figureOutElement($source);
		$destinationCurrentValue = $this->figureOutElement($destination);
		$destinationCurrentValue = intval($destinationCurrentValue);
		$element = intval($element);
		$destinationNewValue = $destinationCurrentValue + $element;
		echo $destinationCurrentValue."-----------".$destinationNewValue."\n\n";
		$storeResponse = $this->storeToDestination($destination , $destinationNewValue);
		$this->setFlags($destination , $instruction , $storeResponse);
		return -1;
	}
}
