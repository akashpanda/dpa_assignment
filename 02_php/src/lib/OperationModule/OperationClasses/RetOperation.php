<?php

class RetOperation extends BaseOperation implements IOperation
{
	public function perform($instruction) {
		$operands = $instruction->detail->x86->operands;
		$toReturnTo = MainMemoryController::get(MainMemoryController::get(RegisterController::get('esp')));
		return $toReturnTo;
	}
}	
