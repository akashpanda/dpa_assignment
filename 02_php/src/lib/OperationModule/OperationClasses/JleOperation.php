<?php

class JleOperation extends BaseOperation implements IOperation
{
	public function perform($instruction) {
		$operands = $instruction->detail->x86->operands;
		$zfValue = RegisterController::getFlag('zf');
		$sfValue = RegisterController::getFlag('sf');
		$ofValue = RegisterController::getFlag('of');
		$operand = $operands[0];
		$value = $this->figureOutElement($operand);
		if($zfValue == 0 && $sfValue != $ofValue) {
			return $value;
		}
		return -1;
	}
}
