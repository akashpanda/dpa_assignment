<?php

class ShlOperation extends BaseOperation implements IOperation
{
	public function perform($instruction) {
		RegisterController::resetFlags();
		$operands = $instruction->detail->x86->operands;
		$destination = $operands[0];
		$supportOperand = $operands[1];
		$element = $this->figureOutElement($destination);
		$shiftTimes = $this->figureOutElement($supportOperand);
		$destinationNewValue = $element << $shiftTimes;
		$storeResponse = $this->storeToDestination($destination , $destinationNewValue);
		$this->setFlags($destination , $instruction , $storeResponse);
		return -1;
	}
}
