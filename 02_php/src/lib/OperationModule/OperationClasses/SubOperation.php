<?php

class SubOperation extends BaseOperation implements IOperation
{
	public function perform($instruction) {
		RegisterController::store('eflags' , 0);
		$operands = $instruction->detail->x86->operands;
		$destination = $operands[0];
		$source = $operands[1];
		$element = $this->figureOutElement($source);
		$destinationCurrentValue = $this->figureOutElement($destination);
		$destinationNewValue = $destinationCurrentValue - $element;
		$storeResponse = $this->storeToDestination($destination , $destinationNewValue);
		$this->setFlags($destination , $instruction , $storeResponse);
		return -1;
	}
}
