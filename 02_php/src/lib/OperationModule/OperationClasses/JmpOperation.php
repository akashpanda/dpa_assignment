<?php

class JmpOperation extends BaseOperation implements IOperation
{
	public function perform($instruction) {
		$operands = $instruction->detail->x86->operands;
		$operand = $operands[0];
		$value = $this->figureOutElement($operand);
		return $value;
	}
}
