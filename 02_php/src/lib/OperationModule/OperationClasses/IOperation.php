<?php

interface IOperation
{
	public function perform($operands) ;
}
