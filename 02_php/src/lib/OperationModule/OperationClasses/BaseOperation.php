<?php

abstract class BaseOperation
{
	protected function figureOutElement($operand , $requiremenLocation = false) {		
//		if(TestVariable::$test == 1) {			print_R($operand);die;		}
		$type = $operand->type;
		$details = $operand->{$type};
		switch($type) {
		case "mem":
			$base = $details->base;
			$baseContent = RegisterController::get($base);
			$size = $operand->size;
			if(TestVariable::$test == 1) {print_R($operand);}
			if(TestVariable::$test == 1) {echo $baseContent."\n\n";}
			$baseContent = $this->getAsPerSize($baseContent , $size , $requiremenLocation);
			//$baseContent = hexdec(bin2hex($baseContent));
//			Debug::getStaticProperties('RegisterController');Debug::getStaticProperties('MainMemoryController'); echo $baseContent;die;
			if(TestVariable::$test == 1) {echo $baseContent."\n\n";}
			if(TestVariable::$test == 1) { Debug::getStaticProperties('RegisterController');Debug::getStaticProperties('MainMemoryController');print_R($baseContent);}
			$actualLocation = $baseContent +  $details->index * $details->scale + $details->disp;
			if($requiremenLocation == true) {
				return $actualLocation;
			}
			return MainMemoryController::get($actualLocation);
		case "imm":
			return $details;
		case "reg":
			return RegisterController::get($details);
		}
	}

	private function getAsPerSize($element , $size , $signed = false) {
		return $element;
		if(!is_integer($element)) {
			return $element;
		}
		if($size == 4 ) {
			return $element;
		}
		return hexdec(bin2hex(int_helper::{"int".(8 * $size).""}($element)));
	}

	protected function storeToDestination($operand , $value ) {
		
//		if(TestVariable::$test == 1) {
//			echo "akash";die;
//		}
		 
		$type = $operand->type;
		$details = $operand->{$type};
		switch($type) {
		case "reg":
			$storeResponse = RegisterController::store($details , $value);
			break;
		case "mem":
			$base = $details->base;
			$baseContent = RegisterController::get($base);
			$size = $operand->size;
			$actualLocation = $baseContent +  $details->index * $details->scale + $details->disp;
			$storeResponse = MainMemoryController::store($actualLocation , $value , $size);
			break;

		}
		return $storeResponse;
	}

	protected function setFlagsByValue($value , $instruction , $flagValues = array()) {
		$flagDetails = $instruction->detail->x86->eflags;
		foreach($flagDetails->modify as $modifyFlags) {
			switch($modifyFlags)  {
			case "cf":
				RegisterController::setFlag($modifyFlags, $flagValues['carryFlag']);
				break;
			case "pf":
				$parityValue = $this->computeParityByValue($value);
				RegisterController::setFlag($modifyFlags, $parityValue);
				break;
			case "af":
				break;
			case "zf":
				$zeroFlag = $this->checkForZeroByValue($value);
				RegisterController::setFlag($modifyFlags, $zeroFlag);
				break;
			case "tf":
				break;
			case "if":
				break;
			case "df":
				break;
			case "sf":
				$signValue = $this->checkSignByValue($value);
				RegisterController::setFlag($modifyFlags, $signValue);
				break;

			}
		}
		foreach($flagDetails->reset as $resetFlag) {
			RegisterController::setFlag($resetFlag, 0);
		}
		foreach($flagDetails->set as $resetFlag) {
                        RegisterController::setFlag($resetFlag, 1);
                }
	}

	protected function setFlags($destination , $instruction  , $flagValues , $destinationType = false) {
		$flagDetails = $instruction->detail->x86->eflags;		
/*		if(!empty($flagDetails->modify)) {
			print_R($flagDetails->modify);
			print_R($flagValues);
			print_R($instruction);die;
		}
 */
		foreach($flagDetails->modify as $modifyFlags) {
			switch($modifyFlags)  {
			case "cf":
				RegisterController::setFlag($modifyFlags, $flagValues['carryFlag']);
				break;
			case "pf":
				$parityValue = $this->computeParity($destination , $destinationType);
				RegisterController::setFlag($modifyFlags, $parityValue);
				break;
			case "af":
				break;
			case "zf":
				$zeroFlag = $this->checkForZero($destination , $destinationType);
				RegisterController::setFlag($modifyFlags, $zeroFlag);
				break;
			case "tf":
				break;
			case "if":
				break;
			case "df":
				break;
			case "sf":
				$signValue = $this->checkSign($destination , $destinationType);
				RegisterController::setFlag($modifyFlags, $signValue);
				break;
			case "of":
				RegisterController::setFlag($modifyFlags, $flagValues['overflow']);
				break;

			}

		}
		foreach($flagDetails->reset as $resetFlag) {
			RegisterController::setFlag($resetFlag, 0);
		}
		foreach($flagDetails->set as $resetFlag) {
                        RegisterController::setFlag($resetFlag, 1);
                }
	}

	private function computeParity($operand , $typeOfOperand) {
		if($typeOfOperand) {
			$type = $typeOfOperand;
			$details = $operand;
		} else {
			$type = $operand->type;
            $details = $operand->{$type};	
		}
		
        switch($type) {
        case "reg":
            $result = RegisterController::computeParity($details);
            break;
        case "mem":
        	$element = $this->figureOutElement($operand);
        	$size = $operand->size;
        	$result = MainMemoryController::computeParityNbits($element , $size*8);
        default:
        }
        return $result;	
	}

	private function checkForZero($destination , $typeOfOperand) {
		if($typeOfOperand) {
			$type = $typeOfOperand;
			$details = $destination;
			switch($type) {
				case "reg":
					return RegisterController::get($details) == 0;
			}
		} else {
			return $this->figureOutElement($destination) == 0;
		}
	}

	private function checkSign($operand , $typeOfOperand) {
		if($typeOfOperand) {
			$type = $typeOfOperand;
			$details = $operand;
		} else {
			$type = $operand->type;
            $details = $operand->{$type};	
		}
                switch($type) {
                case "mem":
                        $base = $details->base;
                        $baseContent = RegisterController::get($base);
                        if($details->index) {
                                $actualLocation = $baseContent + $details->index;
                        }
                        if($details->scale) {
                                $actualLocation = $baseContent * $details->scale;
                        }
                        if($details->disp) {
                                $actualLocation = $baseContent + $details->disp;
                        }
			$result = MainMemoryController::get($actualLocation);
			$result = $result >> 31 & 1;
                case "reg":
                        $result = RegisterController::getSign($details);
		}
		return $result;
	}


	private function computeParityByValue($value) {
		return ($value == 0)?0:1;
	}

	private function checkForZeroByValue($value) {
		return $value == 0;
	}

	private function checkSignByValue($operand) {
		return true;
	}
}
