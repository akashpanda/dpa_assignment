<?php

class FstpOperation extends BaseOperation implements IOperation
{
	public function perform($instruction) {
		$operands = $instruction->detail->x86->operands;
		$destination = $operands[0];
		$top = FloatingPointUnitStackRegister::top();
		$this->storeToDestination($destination , $top);
		FloatingPointUnitStackRegister::pop();
		return -1;
	}
}
