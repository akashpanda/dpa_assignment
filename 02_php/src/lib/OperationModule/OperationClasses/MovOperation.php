<?php

class MovOperation extends BaseOperation implements IOperation
{
	public function perform($instruction) {
		RegisterController::resetFlags();
		$operands = $instruction->detail->x86->operands;
		$destination = $operands[0];
		$source = $operands[1];
		//if(TestVariable::$test == 1) {Debug::getStaticProperties('RegisterController');print_r($operands);die;}
		$element = $this->figureOutElement($source);
		// if(is_numeric($element)) {
		// 	$element = intval($element);
		// }
		$storeResponse = $this->storeToDestination($destination , $element);
		$this->setFlags($destination , $instruction , $storeResponse);
		return -1;
	}
}
