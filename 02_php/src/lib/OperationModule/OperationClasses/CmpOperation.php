<?php

class CmpOperation extends BaseOperation implements IOperation
{
	public function perform($instruction) {
		RegisterController::resetFlags();
		$operands = $instruction->detail->x86->operands;
		$destination = $operands[0];
		$source = $operands[1];
		$element = $this->figureOutElement($source);
		$destinationCurrentValue = $this->figureOutElement($destination);
		if(!is_integer($destinationCurrentValue)) {
			$destinationCurrentValue = ord($destinationCurrentValue);
		}
		if(!is_integer($element)) {
			$element = ord($element);
		}
		$temp = $element - $destinationCurrentValue;
		$this->setFlagsByValue($temp , $instruction);
		return -1;
	}
}
