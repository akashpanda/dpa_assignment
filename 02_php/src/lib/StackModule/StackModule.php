<?php

class StackModule
{
	private static $stack;

	public static function __initializeStack() {
		self::$stack = new SplStack();
	}

	public static function push($element) {
		self::$stack->push($element);
	}

	public static function top() {
		try {
			return self::$stack->top();
		}  catch ( RuntimeException $e) {
                        return StackModuleEmptyStackException();
                }
	}

	public static function pop() {
		try {
			self::$stack->pop();
		} catch ( RuntimeException $e) {
			return StackModuleEmptyStackException();
		}
	}
}


class StackModuleException extends Exception 
{}
class StackModuleEmptyStackException extends StackModuleException
{}
