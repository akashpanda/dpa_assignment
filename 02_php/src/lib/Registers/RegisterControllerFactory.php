<?php

class RegisterControllerFactory
{
	private static $instance;

	protected function __construct() {
	
	}

	public function getInstance() {
		if(!isset(self::$instance)) {
			self::$instance = new RegisterControllerFactory();
		}
		return self::$instance;
	}

	public function getRegisterController() {
		return new RegisterController();
	}
}
