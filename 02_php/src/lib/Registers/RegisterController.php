<?php

class RegisterController
{
	/* 32 bit registers */
	private static  $eax; 
	private static  $ecx;
	private static  $edx;
	private static  $ebx;
	private static  $esp;
	private static  $ebp;
	private static  $esi;
	private static  $edi;

	/* 16 bit registers */
	private static  $ax;
	private static  $cx;
	private static  $dx;
	private static  $bx;
	private static  $sp;
	private static  $bp;
	private static  $si;
	private static  $di;

	/* 8 bit registers */
	private static  $ah , $al;
	private static  $ch , $cl;
	private static  $dh , $dl;
	private static  $bh , $bl;


	/* Segment Registers */
	private static  $ss;
	private static  $cs;
	private static  $ds;
	private static  $es;
	private static  $fs;
	private static  $gs;

	private static  $eflags = 1<<2;
	private static  $eFlags_array = array(
		'cf' => 0, 
		'pf' => 0,
		'af' => 0,
		'zf' => 0,
		'sf' => 0,
		'tf' => 0,
		'if' => 0,
		'df' => 0 
	);
	
	private static  $eip;  //The EIP register contains the address of the next instruction to be executed if no branching is done. EIP can only be read through the stack after a call instruction. 


	public function __construct() {

	}

	public function get($register)	{
		switch($register) {
		case "eax":
		case "ebx":
		case "ecx":
		case "edx":
		case "esp":
		case "ebp":
		case "esi":
		case "edi":
			return self::${$register};
			break;
		case "ax":
		case "bx":
		case "cx":
		case "dx":
		case "sp":
		case "bp":
		case "si":
		case "di":
			$value  =  self::${"e".$register} & bindec("1111111111111111");
			return $value;
			break;
		case "al":
		case "bl":
		case "dl":
			if(!is_integer(self::${"e".$register[0]."x"})) {
				return self::${"e".$register[0]."x"};
			}
			$value = self::${"e".$register[0]."x"} & bindec("11111111");
			return $value;
			break;
		case "ah":
		case "bh":
		case "dh":
			$value = self::${"e".$register[0]."x"} & bindec("1111111100000000");
			$value = $value >> 8;
			return $value;
			break;
		}
	}
	public function store($register , $value) {
		if(!is_int($value)) {
			//self::${$register} = $value;
			//return ;
			
			// if(is_numeric($value)) {
			//   	$value = intval($value);
			//  	echo "1st Block----";
			//  	echo $value."\n\n";
			//  } else {
				$value = ord($value);
			// 	echo "2nd Block----";
			// 	echo $value."\n\n";
			// }
		}
		$overflow = 0;
		//var_dump($register);
		switch($register) {
		case "eax":
		case "ebx":
		case "ecx":
		case "edx":
		case "esp":
		case "ebp":
		case "esi":
		case "edi":
			//			$value = self::handle32bit($value , $overflow);
			$value = $value & bindec('11111111111111111111111111111111');
			self::${$register} = $value;
			break;
		case "ax":
		case "bx":
		case "cx":
		case "dx":
		case "sp":
		case "bp":
		case "si":
		case "di":
			//			$value = self::handle16bit($value , $overflow);
			$value = $value & bindec('1111111111111111');
			self::${"e".$register} =  self::${"e".$register} & bindec("11111111111111110000000000000000");
			self::${"e".$register} =  self::${"e".$register} | $value;
			break;
		case "al":
		case "bl":
		case "dl":
			//			$value = self::handle8bit($value , $overflow);
			$value = $value & bindec('11111111');
			self::${"e".$register[0]."x"} =  self::${"e".$register[0]."x"} & bindec("11111111111111111111111100000000");
			self::${"e".$register[0]."x"} =  self::${"e".$register[0]."x"} | $value;
			break;
		case "ah":
		case "bh":
		case "dh":
			//			$value = self::handle8bit($value , $overflow);
			$value = $value & bindec("11111111");
			self::${"e".$register[0]."x"} =  self::${"e".$register[0]."x"} & bindec("11111111111111110000000011111111");
			$value = $value << 8;
			self::${"e".$register[0]."x"} =  self::${"e".$register[0]."x"} | $value;
			break;
		}
		return array(
			'overflow' => $overflow
		);
	}
	public static function getSign($register) {
		switch($register) {
		case "eax":
		case "ebx":
		case "ecx":
		case "edx":
		case "esp":
		case "ebp":
		case "esi":
		case "edi":
			$registreValue = self::${$register};
			return $registreValue>>31 & 1;
			break;
		case "ax":
		case "bx":
		case "cx":
		case "dx":
		case "sp":
		case "bp":
		case "si":
		case "di":
			$registreValue = self::${$register};
			return $registreValue>>15 & 1;
			break;
		case "al":
		case "ah":
		case "bl":
		case "bh":
		case "dl":
		case "dh":
			$registreValue = self::${$register};
			return $registreValue>>7 & 1;
			break;
		}
	}
	public static function computeParity($register) {
		switch($register) {
                case "eax":
                case "ebx":
                case "ecx":
                case "edx":
                case "esp":
                case "ebp":
                case "esi":
		case "edi":
			return self::computeParityNbits($register , 32);
                        break;
                case "ax":
                case "bx":
                case "cx":
                case "dx":
                case "sp":
                case "bp":
                case "si":
		case "di":
			return self::computeParityNbits($register , 16);
                        break;
                case "al":
                case "ah":
                case "bl":
                case "bh":
                case "dl":
		case "dh":
			return self::computeParityNbits($register , 8);
                        break;
                }

	}
	public static function computeParityNbits($register , $noOfBits) {
		$parity = 0; 
		$n = self::${$register};
		while ($n) 
		{ 
			$parity = !$parity; 
			$n      = $n & ($n - 1); 
		}         
		return $parity; 
	}
	private static  function handle8bit($value , &$overflow) {
		$bitStatus = ($value >> 8) & 1;
		$overflow = $bitStatus;
		return self::handleXbit($value , 8);
	}

	private static  function handle16bit($value , &$overflow) {
		$bitStatus = ($value >> 8) & 1;
                $overflow = $bitStatus;
		return self::handleXbit($value , 16);
	}

	private static  function handle32bit($value, &$overflow) {
		$bitStatus = ($value >> 8) & 1;
                $overflow = $bitStatus;
		return self::handleXbit($value , 32);
	}

	private static  function handleXbit($value , $x) {
		return (pow(2,$x)-1) & $value;
	}

	private static function changeNthFlag($flagValue , $location) {
		if($flagValue == 0) {
			self::$eflags = self::$eflags & ~(1<<$location);
		} else {
			self::$eflags = self::$eflags | (1<<$location);
		}
	}

	private static function getNthBit($content , $bitNo) {
		return ($content >> $bitNo) & 1;
	}

	public static function resetFlags() {
		self::$eFlags_array = array(
			'cf' => 0, 
			'pf' => 0,
			'af' => 0,
			'zf' => 0,
			'sf' => 0,
			'tf' => 0,
			'if' => 0,
			'df' => 0,
			'of' => 0
		);
	}
	public static function getFlag($flagName) {
		if(!isset(self::$eFlags_array[$flagName])) {
			print $flagName."\n";die;
		}
		return self::$eFlags_array[$flagName];
		// $flagValue = self::get('eflags');
		// switch($flagName) {
		// case "cf":
		// 	//0th bit from right is carry flag
		// 	$value = self::getNthBit($flagValue , 0);
		// 	break;
		// case "pf":
		// 	//2rd bit from right is parity flag
		// 	$value = self::getNthBit($flagValue , 2);
		// 	break;
		// case "af":
		// 	//4th bit from right is adjust flag
		// 	$value = self::getNthBit($flagValue , 4);
		// 	break;
		// case "zf":
		// 	//6th bit from right is zero flag
		// 	$value = self::getNthBit($flagValue , 6);
		// 	break;
		// case "sf":
		// 	//7th bit from right is sign flag
		// 	$value = self::getNthBit($flagValue , 7);
		// 	break;
		// case "tf":
		// 	//8th bit from right is trap flag
		// 	$value = self::getNthBit($flagValue , 8);
		// 	break;
		// case "if":
		// 	//9th bit from right is interruption flag
		// 	$value = self::getNthBit($flagValue , 9);
		// 	break;
		// case "df":
		// 	//10th bit from right is direction flag 
		// 	$value = self::getNthBit($flagValue , 10);
		// 	break;
		// case "of":
		// 	break;
		// case "iopl":
		// 	break;
		// case "nt":
		// 	break;
		// case "rf":
		// 	break;
		// case "vm":
		// 	break;
		// case "ac":
		// 	break;
		// case "vif":
		// 	break;
		// case "vip":
		// 	break;
		// }
		// return $value;
	}
	public static function setFlag($flagName , $flagValue) {
		//https://en.wikibooks.org/wiki/X86_Assembly/X86_Architecture : Flag structure can be found here 
		self::$eFlags_array[$flagName] = $flagValue;
		// switch($flagName) {
		// case "cf":
		// 	//0th bit from right is carry flag
		// 	self::changeNthFlag($flagValue , 0);
		// 	break;
		// case "pf":
		// 	//2rd bit from right is parity flag
		// 	self::changeNthFlag($flagValue , 2);
		// 	break;
		// case "af":
		// 	//4th bit from right is adjust flag
		// 	self::changeNthFlag($flagValue , 4);
		// 	break;
		// case "zf":
		// 	//6th bit from right is zero flag
		// 	self::changeNthFlag($flagValue , 6);
		// 	break;
		// case "sf":
		// 	//7th bit from right is sign flag
		// 	self::changeNthFlag($flagValue , 7);
		// 	break;
		// case "tf":
		// 	//8th bit from right is trap flag
		// 	self::changeNthFlag($flagValue , 8);
		// 	break;
		// case "if":
		// 	//9th bit from right is interruption flag
		// 	self::changeNthFlag($flagValue , 9);
		// 	break;
		// case "df":
		// 	//10th bit from right is direction flag 
		// 	self::changeNthFlag($flagValue , 10);
		// 	break;
		// case "of":
		// 	break;
		// case "iopl":
		// 	break;
		// case "nt":
		// 	break;
		// case "rf":
		// 	break;
		// case "vm":
		// 	break;
		// case "ac":
		// 	break;
		// case "vif":
		// 	break;
		// case "vip":
		// 	break;

		//}
	}

}	
