<?php
require_once(dirname(__FILE__)."/vendor/autoload.php");

function string_hex($bytes) {
    if (is_string($bytes)) {
        $bytes = unpack('C*', $bytes);
    }
    return implode(" ",
        array_map(function($x) {
            return is_int($x) ? sprintf("0x%02x", $x) : $x;
            },
            $bytes
        )
    );
};
$filename = $argv[1];
$handle = fopen($filename, "r");
$contents = fread($handle, filesize($filename));
fclose($handle);

$emulator = EmulationFactory::getInstance()->getX86_32EmulationManager(); 
$emulator->executeCode($contents , 0x0000 , $argv);
print_R($inst);die;

